from django.conf.urls import patterns, include, url
from django.contrib import admin
from eArmoury import views

urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.home, name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^players/', include('matchmaking.urls')),
    url(r'^accounts/register/$', views.CustomRegistrationView.as_view(), name='registration_register'),
    url(r'^accounts/', include('registration.auth_urls')),
)


