from django.shortcuts import render
from registration.backends.simple.views import  RegistrationView
from matchmaking.models import Player, Role
from matchmaking import settings
from django.db.models import Avg, Count

def home(request):
    NUM_LEADERS = 5
    #Players below the threshold rating count cannot be on the leaderboards
    old_players = Player.objects.all().annotate(num_ratings=Count('ratings'))\
                        .filter(num_ratings__gte=settings.THRESHOLD_RATING_COUNT)\
                        .values_list('pk',flat=True)

    friendliness_leaders = Player.objects.filter(pk__in=old_players)\
                                         .annotate(fr = Avg("ratings__friendliness"))\
                                         .order_by('-fr')[:NUM_LEADERS]
    teamwork_leaders     = Player.objects.filter(pk__in=old_players)\
                                         .annotate(tr = Avg("ratings__teamwork"))\
                                         .order_by('-tr')[:NUM_LEADERS]
    reliability_leaders  = Player.objects.filter(pk__in=old_players)\
                                         .annotate(rr = Avg("ratings__reliability"))\
                                         .order_by('-rr')[:NUM_LEADERS]

    latest_players =Player.objects.order_by('-pk')[:5]
    return render(request, 'home.html', {
        "friendliness_players":friendliness_leaders,
        "teamwork_players":teamwork_leaders,
        "reliability_players":reliability_leaders,
        "roles":Role.objects.all(),
        "latest_players":latest_players,
        })

class CustomRegistrationView(RegistrationView):
    disallowed_url = 'player_update'
   
    def get_success_url(self, request, user):
        return 'player_update'

    #you cant register if you are logged in
    def registration_allowed(self, request):
        return not request.user.is_authenticated()
