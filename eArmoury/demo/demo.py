import random
import os
import sys

# Add path to settings.py file to sys path
here = (os.path.abspath(__file__))
settings = os.path.normpath(os.path.join(here,'../../../'))
sys.path.append(settings)

# Set enviornment settings
os.environ['DJANGO_SETTINGS_MODULE'] = 'eArmoury.settings'

# Needs to be called 
import django
django.setup()
from django.contrib.auth.models import User
from matchmaking.models import *

message = """Albert Einstein, one of the greatest minds to ever live, was once perplexed by the speed at which Arteezy was able to farm. Seemingly impossible GPMs of 800+ every game were consistently observed, leading to the hypothesis that this was no fluke. Einstein spent the last years of his life trying to prove the ability of Arteezy's farm existed within the laws of science.
"""

players = [
        ("Dendi",76561198030654385, 70388657),
        ("AdmiralBulldog",76561198036748162, 76482434),
        ("Arteezy",76561198047011640, 86745912),
        ("Ferrari_430",76561198048850805,88585077),
        ("loda",76561198061761348,101495620),
        ("Black",76561197981555031, 21289303),
        ("s4",76561198001497299, 41231571),
        ("Aui_2000",76561198000813202, 40547474),
        ("w33",76561198046966189, 86700461),
        ("WagaGaming",76561197993261133, 32995405),
        ("Fear",76561198047443319, 87177591),
        ("SingSing",76561197980022982, 19757254),
        ("XBOCT",76561198049891200,89625472 ),
        ("Meracle", 76561198051635104, 91369376),
        ("EE",76561198003541947, 89625472),
        ("Akke",76561198001554683, 41288955),
        ("BigDaddy",76561197979938082, 19672354),
        ("Illidan",76561197986582419, 26316691),
        ("Puppey", 76561198047544485, 87278757),
        ("EGM",76561197964182156, 3916428),
        ("jeraxai",76561197987037722, 26771994),
        ("iceiceice",76561198045038168,84772440),
        ("kky",76561198042528392, 82262664),
        ("Purge",76561198026562132, 66296404),
        ("Merlini",76561198028025765,67760037),
        ("Funn1k",76561198046988871,86723143),
        ("xcalibur",76561198089876405,129610677),
        ("Fenrir",76561198074066546,113800818),
        ("BurNIng",76561198051158462,90892734),
        ("JotM",76561197996813539,36547811),
        ("1437",76561198047462618,87196890),
        ("Universe",76561198047542075,87276347),
        ("Zsmj",76561198073971421,113705693),
        ("SmAsh",76561198078339297,118073569),
        ("Trixi",76561197968978034,8712306),
        ("Zai",76561198033828054,73562326),
        ("FATA",76561198047065028,86799300),
        ("Yao",76561198059143738,98878010),
        ("ppd", 76561198046993283,86727555),
        ("YYF",76561198050310737,90045009)]

ROLES = ['Carry', 'Middle', 'Offlane', 'Jungler', 'Support']
SERVERS = [ 'EU West', 'EU East', 'Russia', ]
LANGS = [ ('English','English'), ('Spanish','Español'), ('Russian','Русский'),]

def load_roles():
    for role in ROLES:
        Role.objects.get_or_create(name=role)

def load_servers():
    for server in SERVERS:
        Server.objects.get_or_create(name=server)

def load_languages():
    for eng_ver,lang in LANGS:
        Language.objects.get_or_create(name=lang, english_version=eng_ver)

def add_players():
    for username,steam_id,dotabuff in players:
        user = User.objects.create_user(username=username,email=username+"@example.com",password="password")
        p = Player.objects.create(
                    user = user,
                    steam_id=steam_id,
                    dota_buff_profile=dotabuff,
                    mmr = random.randint(300, 7000),
                    ranked = bool(random.randint(0,4)),
                    av_weekdays = bool(random.getrandbits(1)),
                    av_weeknights = bool(random.getrandbits(1)),
                    av_weekends = bool(random.getrandbits(1)),
                    mic = bool(random.getrandbits(1)),
                    message = message,
                    include_in_search = bool(random.randint(0,7))
                )
        for role in random.sample([1,2,3,4,5],random.randint(1,5)):
            p.roles.add(Role.objects.get(pk=role))

        for language in random.sample([1,2,3],random.randint(1,3)):
            p.languages.add(Language.objects.get(pk=language))

        for server in random.sample([1,2,3], random.randint(1,3)):
            p.servers.add(Server.objects.get(pk=server))

def add_ratings():
    #The last 5 will be new players
    for player in Player.objects.all()[:35]:
        for user in User.objects.all():
            if not player.user == user and not bool(random.randint(0,2)):
                    PlayerRating.objects.create(critic=user, target_player=player, 
                        friendliness=random.randint(1,5),
                        teamwork=random.randint(1,5),
                        reliability=random.randint(1,5))


def add_saves():
    for user in User.objects.all():
        if hasattr(user, 'player'):
            saved = random.sample(list(Player.objects.all().exclude(pk = user.player.pk).values_list('pk', flat=True)), random.randint(1,10))
        else:
            saved = random.sample(list(Player.objects.all().values_list('pk', flat=True)), random.randint(1,10))
        for p in saved:
            PlayerSave.objects.create(critic=user, target_player=Player.objects.get(pk=p))


if __name__ == '__main__':
    print("Adding Roles...")
    load_roles()
    print("Adding Languages...")
    load_languages()
    print("Adding Servers...")
    load_servers()
    print("Adding Users + Players...")
    add_players()
    print("Adding Ratings...")
    add_ratings()
    print("Adding PlayerSaves...")
    add_saves()
