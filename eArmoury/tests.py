from django.test import TestCase, Client
from django.utils import unittest

class AuthTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_register_redirect(self):
        response = self.client.post('/accounts/register/',
                {
                    'username':"user001",
                    'email':"test@example.com",
                    'password1':"password",
                    'password2':"password",
                })
        self.assertTrue(response.url,'/players/update/')

