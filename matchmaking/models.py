from django.db import models
from django.core.urlresolvers import reverse
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from django.db.models import Avg
from matchmaking import settings


class Role(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class Server(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class Language(models.Model):
    name = models.CharField(max_length=100)
    english_version = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Player(models.Model):

    user = models.OneToOneField(User)
    steam_id = models.BigIntegerField(
        verbose_name="SteamID64",
        help_text='The 17 digit number following your community profile. You may find this <a taget="_" href="http://steamidconverter.com/"> id converter </a> useful.',
        )

    dota_buff_profile = models.CharField(
        max_length=20,
        verbose_name="Dota Buff Profile Number",
        help_text="http://www.dotabuff.com/players/&ltID NUMBER&gt/",
        blank=True,  # A few people wont have dotabuff profiles.
        )

    #mmr should be updated atleast every month
    mmr = models.PositiveSmallIntegerField(
        verbose_name="MMR",
        validators=[
            MaxValueValidator(10000),
            MinValueValidator(0)])
    mmr_date = models.DateField(auto_now=True)
    ranked = models.BooleanField(default=True)
    roles = models.ManyToManyField(Role)
    servers = models.ManyToManyField(Server)
    languages = models.ManyToManyField(Language)
    av_weekdays = models.BooleanField(default=False, verbose_name="Weekdays")
    av_weeknights = models.BooleanField(default=False, verbose_name="Weeknights")
    av_weekends = models.BooleanField(default=False, verbose_name="Weekends")
    mic = models.BooleanField(default=False, verbose_name="Microphone?")
    message = models.TextField(max_length=500, blank=True, verbose_name="Personal Message",
                               help_text=("Up to 500 Characters on what you are looking for"
                                          "in a team."))
    include_in_search = models.BooleanField(default=True, verbose_name="Included in player search results")

    def is_new(self):
        return self.ratings.count() < settings.THRESHOLD_RATING_COUNT 

    #rankings use average
    def get_friendliness(self):
        return self.ratings.aggregate(Avg('friendliness'))['friendliness__avg']

    def get_teamwork(self):
        return self.ratings.aggregate(Avg('teamwork'))['teamwork__avg']

    def get_reliability(self):
        return self.ratings.aggregate(Avg('reliability'))['reliability__avg']


    def __str__(self):
        return self.user.username 

    def get_absolute_url(self):
        return reverse('player_detail', kwargs={'pk': self.pk})

class PlayerSave(models.Model):
    critic = models.ForeignKey(User, related_name="players_saved")
    target_player = models.ForeignKey(Player, related_name="saved_by")
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return '%s --> %s' %(self.critic.username, self.target_player.user.username)

class PlayerRating(models.Model):
    # critic is related to a User model because they dont need to add info to use search and rate
    # target_player is related to a Player because they must have added info to be found
    # and  therefor rated.
    critic = models.ForeignKey(User, related_name="ratings_made")
    target_player = models.ForeignKey(Player, related_name="ratings")
    friendliness = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(0)])
    teamwork = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(0)])
    reliability = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(0)])

    def __str__(self):
        return '%s --> %s' %(self.critic.username, self.target_player.user.username)

    class Meta:
        unique_together = ("critic", "target_player")
