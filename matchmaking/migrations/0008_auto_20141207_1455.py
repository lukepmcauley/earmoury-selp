# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('matchmaking', '0007_player_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='include_in_search',
            field=models.BooleanField(default=True, verbose_name='Included in player search results'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='player',
            name='message',
            field=models.TextField(help_text='Up to 500 Characters on what you are looking forin a team.', max_length=500, verbose_name='Personal Message', blank=True),
        ),
        migrations.AlterField(
            model_name='player',
            name='mmr',
            field=models.PositiveSmallIntegerField(verbose_name='MMR', validators=[django.core.validators.MaxValueValidator(10000), django.core.validators.MinValueValidator(0)]),
        ),
    ]
