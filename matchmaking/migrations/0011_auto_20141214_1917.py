# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('matchmaking', '0010_auto_20141210_1946'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlayerSave',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('date_created', models.DateField(auto_now_add=True)),
                ('critic', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='players_saved')),
                ('target_player', models.ForeignKey(to='matchmaking.Player', related_name='saved_by')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='player',
            name='steam_id',
            field=models.BigIntegerField(help_text='The 17 digit number following your community profile. You may find this <a taget="_" href="http://steamidconverter.com/"> id converter </a> useful.', verbose_name='SteamID64'),
        ),
    ]
