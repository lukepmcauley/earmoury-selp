# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matchmaking', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='player',
            name='availability',
        ),
        migrations.RemoveField(
            model_name='player',
            name='steam_64id',
        ),
        migrations.AddField(
            model_name='player',
            name='steam_id',
            field=models.CharField(max_length=200, default='turkz'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='role',
            name='short',
            field=models.CharField(max_length=3, default='EUW'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='player',
            name='dota_buff_profile',
            field=models.CharField(verbose_name='Dota Buff Profile Number', max_length=20, blank=True, help_text='http://www.dotabuff.com/players/&ltID NUMBER&gt/'),
        ),
    ]
