# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matchmaking', '0004_auto_20141127_2353'),
    ]

    operations = [
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=100)),
                ('english_version', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='player',
            name='languages',
            field=models.ManyToManyField(to='matchmaking.Language'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='player',
            name='message',
            field=models.TextField(blank=True, verbose_name='Personal Message', help_text='Up to 500 Characters on what you are looking for in a team.', default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='player',
            name='mic',
            field=models.BooleanField(verbose_name='Microphone?', default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='player',
            name='av_weekdays',
            field=models.BooleanField(verbose_name='Weekdays', default=False),
        ),
        migrations.AlterField(
            model_name='player',
            name='av_weekends',
            field=models.BooleanField(verbose_name='Weekends', default=False),
        ),
        migrations.AlterField(
            model_name='player',
            name='av_weeknights',
            field=models.BooleanField(verbose_name='Weeknights', default=False),
        ),
    ]
