# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('matchmaking', '0009_auto_20141208_0013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='steam_id',
            field=models.BigIntegerField(),
        ),
        migrations.AlterField(
            model_name='playerrating',
            name='critic',
            field=models.ForeignKey(related_name='ratings_made', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='playerrating',
            name='target_player',
            field=models.ForeignKey(related_name='ratings', to='matchmaking.Player'),
        ),
    ]
