# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('steam_64id', models.CharField(help_text='http://www.dotabuff.com/players/&ltID NUMBER&gt/', verbose_name='Steam ID Number', max_length=20)),
                ('dota_buff_profile', models.CharField(help_text='http://steamcommunity.com/profiles/&ltID NUMBER/', blank=True, verbose_name='Dota Buff Profile Number', max_length=20)),
                ('mmr', models.PositiveSmallIntegerField(verbose_name='MMR')),
                ('mmr_date', models.DateField(auto_now=True)),
                ('availability', models.CharField(help_text='<a href="http://en.wikipedia.org/wiki/List_of_UTC_time_offsets"target = "_blank">Time Zone help </a>', max_length=1, verbose_name='Times available in UTC', choices=[('0', '00:00 - 04:00'), ('1', '04:00 - 08:00'), ('2', '08:00 - 12:00'), ('3', '12:00 - 16:00'), ('4', '16:00 - 20:00'), ('5', '20:00 - 24:00')])),
                ('ranked', models.BooleanField(default=True)),
                ('unranked', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Server',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='player',
            name='roles',
            field=models.ManyToManyField(to='matchmaking.Role'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='player',
            name='servers',
            field=models.ManyToManyField(to='matchmaking.Server'),
            preserve_default=True,
        ),
    ]
