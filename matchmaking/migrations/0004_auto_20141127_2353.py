# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matchmaking', '0003_remove_role_short'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='av_weekdays',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='player',
            name='av_weekends',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='player',
            name='av_weeknights',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
