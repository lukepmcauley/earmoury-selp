# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matchmaking', '0005_auto_20141201_1409'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='player',
            name='unranked',
        ),
    ]
