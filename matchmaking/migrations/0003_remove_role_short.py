# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matchmaking', '0002_auto_20141127_2332'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='role',
            name='short',
        ),
    ]
