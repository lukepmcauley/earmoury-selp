from django.views.generic import ListView, DetailView, CreateView, FormView
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator,PageNotAnInteger, EmptyPage
from operator import itemgetter
from matchmaking.models import Player, PlayerRating, Role, PlayerSave
from django.db.models import Avg, Count
from matchmaking.forms import *
from matchmaking import settings, steam_api
import json

@ensure_csrf_cookie
def player_detail(request, pk="1"):
    """ This view displays the profile of a given player and handles ratings form.

    The pk argument is the pk of the player being viewed.
    The rating form is only shown to users whom are logged in and are not viewing their own profile.
    """
    template = 'matchmaking/player_detail.html'

    target_player = get_object_or_404(Player, pk=pk)
    friendliness = target_player.get_friendliness()
    teamwork = target_player.get_teamwork()
    reliability = target_player.get_reliability()
    context = {
            'player':target_player,
            'friendliness':friendliness,
            'teamwork':teamwork,
            'reliability':reliability,
            'show_form':True,
            'saved':False,
    }

    #Set up the save button 
    if request.user.is_authenticated():
        if request.user.players_saved.filter(target_player=target_player):
            context['saved'] = True

    if request.method == 'POST':
        form = RatingForm(request.POST)
        if form.is_valid():
            #update or add rating 
            if PlayerRating.objects.filter(
                    critic=request.user,
                    target_player=target_player,
                    ).exists():
                PlayerRating.objects.filter(
                    critic=request.user,
                    target_player=target_player,
                    ).update(
                        friendliness=form['friendliness'].value(),
                        teamwork=form['teamwork'].value(),
                        reliability=form['reliability'].value(),
                        )
            else:
                PlayerRating.objects.create(
                    critic=request.user,
                    target_player=target_player,
                    friendliness=form['friendliness'].value(),
                    teamwork=form['teamwork'].value(),
                    reliability=form['reliability'].value(),
                    )
            return HttpResponseRedirect(reverse('player_detail', kwargs={'pk':target_player.pk}))
        else:
            #There was an error with the form 
            context['form'] = form
            return render(request, template, form)            
    else:
        if request.user.is_authenticated() and not request.user == target_player.user:
            # A user is loggged in. They are shown a form with their ratings for the 
            # Player or a blank on if they have never rated the player
            data = dict()
            if PlayerRating.objects.filter(critic=request.user,target_player=target_player).exists():
                rating = PlayerRating.objects.get(
                                        critic=request.user,
                                        target_player=target_player)
                data['friendliness'] = rating.friendliness
                data['teamwork'] = rating.teamwork
                data['reliability'] = rating.reliability
                context['form'] = RatingForm(data)
            else:
                context['form'] = RatingForm()
            return render(request,template , context)
        else:
            #A user is viewing their own profile, so they cant submit ratings
            context['show_form'] = False
            return render(request, template , context)

@login_required
def player_create_or_update(request):
    """ This view allows users to create a player profile or update and existing one."""
    if request.method == 'POST':
        if hasattr(request.user, 'player'):
            form = PlayerForm(request.POST, instance=request.user.player)
        else:
            form = PlayerForm(request.POST)

        if form.is_valid():
            player = form.save(commit=False)
            player.user = request.user
            player.save()
            form.save_m2m()
            return HttpResponseRedirect(reverse('player_detail', kwargs={'pk':player.pk}))
    else:
        if hasattr(request.user,'player'):
            form = PlayerForm(instance=request.user.player)
        else:
            form = PlayerForm()

    return render(request, 'matchmaking/player_form.html', {'form' :form})

def get_steam_stats(request, pk):
    if pk:
        try:
            api = steam_api.SteamAPI(settings.STEAM_API_KEY)
            json_data = api.get_player_summaries([Player.objects.get(pk=pk).steam_id])
            steam_stats = json.loads(json_data)['response']['players'][0]
            if steam_stats['personastate'] == 1 or steam_stats['personastate'] == 6:
                steam_stats['online'] = True
            else:
                steam_stats['online'] = False
            steam_stats['success'] = True
            return JsonResponse(steam_stats)
        except:
            pass
    return JsonResponse({'success':False})

@login_required
def player_saved(request):
    """This view displays a login in players saved players"""
    players = [sp.target_player for sp in request.user.players_saved.all().order_by('-date_created')]
    context = {'players': players}
    return render(request, 'matchmaking/player_saved.html', context)

def save_player(request):
    """Hanldes ajax request that adds player to a users saved list"""
    if request.method == 'POST':
        form = GetPlayer(request.POST)
        if request.user.is_authenticated and form.is_valid():
            try:
                PlayerSave.objects.get_or_create(critic=request.user,
                    target_player=Player.objects.get(pk=form['target_player'].value()))
                return JsonResponse({'success':True})
            except:
                pass
    return JsonResponse({'success':False})

def forget_player(request):
    """Hanldes ajax request that deletes player from a users saved list"""
    if request.method == 'POST':
        form = GetPlayer(request.POST)
        if request.user.is_authenticated and form.is_valid():
            try:
                player = PlayerSave.objects.get(critic=request.user,
                    target_player=Player.objects.get(pk=form['target_player'].value()))
                player.delete()
                return JsonResponse({'success':True})
            except:
                pass
    return JsonResponse({'success':False})

def player_search(request):
    """ This view handles the search form and results.

    Players are first filter to match the users search.
    Then users with more than settings.THRESHOLD_RATING_COUNT ratings 
    are ranked by taking the dot product of the user supplied weights and their
    average rating.
    New players without enough ratings to be ranked are included in the results at
    odd positions starting at the 3rd position.
    """
    template = 'matchmaking/player_search.html'

    #Check if there is a search has been made
    if request.method == 'GET' and request.GET.dict():
        form = SearchForm(request.GET)
        if form.is_valid():
            players = Player.objects.filter(
                include_in_search=True,
                mmr__gte=form['min_mmr'].value(),
                mmr__lte=form['max_mmr'].value(),
                servers__in=form['servers'].value(),
                languages__in=form['languages'].value(),
                roles__in=form['roles'].value(),
                ).distinct()

            #An unchecked textbox means the attribute matters (Not that the attribute must equal false).
            if form['ranked'].value():
                players = players.filter(ranked=True)
            if form['microphone'].value():
                players = players.filter(mic=True)
            if form['av_weekdays'].value():
                players = players.exclude(av_weekdays=False)
            if form['av_weeknights'].value():
                players = players.exclude(av_weeknights=False)
            if form['av_weekends'].value():
                players = players.exclude(av_weekends=False)

            # Do a second query instead of nesting
            players = players.values_list('pk', flat=True)
            # new players dont have enough ratings
            old_players = Player.objects.filter(pk__in=players)\
                                .annotate(num_ratings=Count('ratings'))\
                                .filter(num_ratings__gte=settings.THRESHOLD_RATING_COUNT)

            new_players = Player.objects.filter(pk__in=players)\
                                .annotate(num_ratings=Count('ratings'))\
                                .filter(num_ratings__lt=settings.THRESHOLD_RATING_COUNT)

            player_scaled_ratings = []
            for player in old_players:
                total_rating = player.get_friendliness() * float(form['friendliness_weight'].value())
                total_rating += player.get_teamwork() * float(form['teamwork_weight'].value())
                total_rating += player.get_reliability() * float(form['reliability_weight'].value())

                player_scaled_ratings.append((player, total_rating))
            sorted_players = sorted(player_scaled_ratings, key=itemgetter(1), reverse=True)

            players = [p for  p, _ in sorted_players]

            #mixes old players in starting from the 3rd player
            position = 2
            for player in new_players:
                players.insert(position, player)
                position +=2

            paginator = Paginator(players, settings.PAGINATION_ITEMS)

            page = request.GET.get('page')
            try:
                players = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                players = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                players = paginator.page(paginator.num_pages)


            return render(request,
                          template,
                          {
                                'form':form,
                                'players':players,
                                'roles':Role.objects.all(),
                                'show_list':True,
                            })
        else:
            return render(request,template, {'form':form,'show_list':False })

    else:
        # return the empty search form 
            return render(request, template, {'form':SearchForm(),'show_list':False })
