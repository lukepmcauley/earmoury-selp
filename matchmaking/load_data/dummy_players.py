import django_settings

from matchmaking.models import *
from django.contrib.auth.models import User

message = "Hello, dendi face (no space)"

PLAYERS = {
        "antimage":{
            "steam_id":"76561198009347080",
            "dota_buff_profile":111111,
            "ranked":True,
            "mmr":1000,
            "av_weekdays":True,
            "av_weeknights":True,
            "av_weekends":True,
            "mic":True,
            "message":message,
            "roles":["Carry"],
            "servers":["Russia"],
            "languages":["Russian"],
            "include_in_search":True,
            },
        "axe":{
            "steam_id":"76561198004175777",
            "dota_buff_profile":22222,
            "ranked":False,
            "mmr":2000,
            "av_weekdays":True,
            "av_weeknights":True,
            "av_weekends":True,
            "mic":True,
            "message":message,
            "roles":["Support"],
            "servers":["EU West"],
            "languages":["English"],
            "include_in_search":True,
            },
        "enigma":{
            "steam_id":"76561198013740235",
            "dota_buff_profile":333333,
            "ranked":True,
            "mmr":3000,
            "av_weekdays":False,
            "av_weeknights":True,
            "av_weekends":True,
            "mic":False,
            "message":message,
            "roles":["Jungler"],
            "servers":["Dubai"],
            "languages":["Spanish"],
            "include_in_search":True,
            },
        "tinker":{
            "steam_id":"76561197974892779",
            "dota_buff_profile":444444,
            "ranked":True,
            "mmr":6000,
            "av_weekdays":True,
            "av_weeknights":False,
            "av_weekends":False,
            "mic":True,
            "message":message,
            "roles":["Carry","Jungler"],
            "servers":["US West","US East"],
            "languages":["English"],
            "include_in_search":True,
            },
        "queenofpain":{
            "steam_id":"76561198011501143",
            "dota_buff_profile":1212121,
            "ranked":True,
            "mmr":1000,
            "ranked":True,
            "av_weekdays":False,
            "av_weeknights":False,
            "av_weekends":True,
            "mic":True,
            "message":message,
            "roles":["Mid"],
            "servers":["EU East"],
            "languages":["Spanish", "English"],
            "include_in_search":True,
            },
        "riki":{
            "steam_id":"76561198002037891",
            "dota_buff_profile":211111,
            "ranked":True,
            "mmr":1000,
            "av_weekdays":True,
            "av_weeknights":True,
            "av_weekends":True,
            "mic":True,
            "message":message,
            "roles":["Carry"],
            "servers":["Russia"],
            "languages":["Russian"],
            "include_in_search":False,
            },
        }

from django.contrib.auth.models import User

def load_users():

    for name in PLAYERS.keys():
        if(not User.objects.filter(username=name).exists()):
            User.objects.create_user(name, name+'@email.com', 'password')

# Options must be loaded first!
def load_players():

    for username in PLAYERS.keys():
                player = PLAYERS[username]
                p,_ = Player.objects.get_or_create(
                    user = User.objects.get(username=username),
                    steam_id = player['steam_id'],
                    dota_buff_profile = player['dota_buff_profile'],
                    mmr = player['mmr'],
                    ranked = player['ranked'],
                    av_weekdays = player['av_weekdays'],
                    av_weeknights = player['av_weeknights'],
                    av_weekends = player['av_weekends'],
                    mic = player['mic'],
                    message = player['message'],
                    include_in_search = player['include_in_search'],
                    )
                for role in player['roles']:
                    p.roles.add(Role.objects.get(name=role))

                for server in player['servers']:
                    p.servers.add(Server.objects.get(name=server))

                for lang in player['languages']:
                    p.languages.add(Language.objects.get(english_version=lang))


if __name__ == "__main__":
    load_users()
    load_players()
