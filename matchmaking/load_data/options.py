import django_settings
from matchmaking.models import Role, Server, Language
ROLES = ['Carry', 'Mid', 'Offlane', 'Jungler', 'Support']
SERVERS = [
            'EU West',
            'EU East',
            'Russia',
            'US West',
            'US East',
            'South America',
            'Chilie',
            'South Africa',
            'Dubai',
            'South East Asia',
            'Peru',
            'Australia',
        ]

LANGS = [
        ('English','English'),
        ('Spanish','Español'),
        ('Russian','Русский'),
        ('Ukrainian','Українська'),  
        ('German','Deutsch'),
        ('Swedish','Svenska'),
        ('Chinese','中文'),
        ('Korean','한국어'),
        ('Indonesian','Bahasa Indonesia'),
        ('Klingon','tlhIngan Hol'),
        ]

def load_roles():
    for role in ROLES:
        Role.objects.get_or_create(name=role)

def load_servers():
    for server in SERVERS:
        Server.objects.get_or_create(name=server)

def load_languages():
    for eng_ver,lang in LANGS:
        Language.objects.get_or_create(name=lang, english_version=eng_ver)


if __name__ == "__main__":
    load_roles()
    load_servers()
    load_languages()
