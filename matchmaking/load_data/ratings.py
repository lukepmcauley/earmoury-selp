import django_settings
from django.contrib.auth.models import User
from matchmaking.models import Player, PlayerRating
from dummy_players import PLAYERS
from random import randrange

def load_ratings():
    print("loading ratings")
    for critic in PLAYERS.keys():
        for target in PLAYERS.keys():
            if not critic == target:
                if not PlayerRating.objects.filter(
                        critic=User.objects.get(username=critic),
                        target_player=Player.objects.get(user__username=target),
                        ).exists():
                    PlayerRating.objects.create(
                        critic=User.objects.get(username=critic),
                        target_player=Player.objects.get(user__username=target),
                        friendliness=randrange(0,6),
                        teamwork=randrange(0,6),
                        reliability=randrange(0,6)
                        )


if __name__ == "__main__":
    load_ratings()
