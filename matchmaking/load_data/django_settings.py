import os
import sys
import django

# Add path to settings.py file to sys path
here = (os.path.abspath(__file__))
settings = os.path.normpath(os.path.join(here,'../../../'))
sys.path.append(settings)

# Set enviornment settings
os.environ['DJANGO_SETTINGS_MODULE'] = 'eArmoury.settings'

# Needs to be called 
django.setup()
