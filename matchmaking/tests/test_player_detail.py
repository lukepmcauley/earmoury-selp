from django.test import TestCase, Client
from matchmaking.models import Player
import json

class TestGetSteamStats(TestCase):
    """Test the get_steam_stats view """
    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()

    def test_normal_case(self):
        pk = 1
        response = self.client.get('/players/'+str(pk)+'/steam_stats/')
        data = json.loads(response.content.decode('utf-8'))
        self.assertTrue(data['success'])
        self.assertTrue(Player.objects.get(pk=pk).steam_id, data['steamid'])

    def test_invalid_pk(self):
        pk = 1000000
        response = self.client.get('/players/'+str(pk)+'/steam_stats/')
        data = json.loads(response.content.decode('utf-8'))
        self.assertFalse(data['success'])



