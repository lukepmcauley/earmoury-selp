from django.test import TestCase, Client
from matchmaking import settings, steam_api 
import json

class TestGetPlayerSummaries(TestCase):
    """ Test the get_player_summaries method of the steam_api"""
    fixtures = ['test_data.json']

    def setUp(self):
        self.api = steam_api.SteamAPI(settings.STEAM_API_KEY)

    def test_one_player(self):
        """ test if a request for info on 1 steam id returns a valid response"""
        data = json.loads(self.api.get_player_summaries([76561198009347080]))
        self.assertEqual(data['response']['players'][0]['steamid'], '76561198009347080')


    def test_multiple_players(self):
        """ test if a request for info on 3 steam ids returns a valid response"""
        data = json.loads(self.api.get_player_summaries(
            [
                76561198009347080,
                76561198047011640,
                76561197993261133,
            ]))
        response_ids = [player['steamid'] for player in data['response']['players']] 
        self.assertTrue('76561198009347080' in response_ids)
        self.assertTrue('76561198047011640' in response_ids)
        self.assertTrue('76561197993261133' in response_ids)

    def test_no_players(self):
        data = self.api.get_player_summaries([])
        self.assertEqual(data, None)
