from django.test import TestCase, Client
from matchmaking.models import *
from operator import itemgetter

class PlayerSearchTest(TestCase):
    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()

    def contains_player(self, players, username):
        for player in players:
            if player.user.username == username:
                return True
        return False

    def test_player_list_status_code(self):
        response = self.client.get('/players/')
        self.assertEqual(response.status_code, 200)


    def test_max_mmr(self):
        response = self.client.get('/players/', {
            'min_mmr':0,
            'max_mmr':5500,
            'roles':(Role.objects.get(name='Mid').pk, Role.objects.get(name='Carry').pk),
            'servers':(Server.objects.get(name='EU East').pk,
                       Server.objects.get(name="US East").pk),
            'languages':(Language.objects.get(english_version="English").pk,),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'], "queenofpain"))
        self.assertFalse(self.contains_player(response.context['players'], "tinker"))

    def test_min_mmr(self):
        response = self.client.get('/players/', {
            'min_mmr':3000,
            'max_mmr':10000,
            'roles':(Role.objects.get(name='Mid').pk, Role.objects.get(name='Carry').pk),
            'servers':(Server.objects.get(name='EU East').pk,
                       Server.objects.get(name='US East').pk),
            'languages':(Language.objects.get(english_version='English').pk,),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'], "tinker"))
        self.assertFalse(self.contains_player(response.context['players'], "queenofpain"))

    def test_must_be_ranked(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'ranked':True,
            'roles':(Role.objects.get(name='Support').pk, Role.objects.get(name='Carry').pk),
            'servers':(Server.objects.get(name='EU West').pk, Server.objects.get(name="Russia").pk),
            'languages':(Language.objects.get(english_version="Russian").pk,
                         Language.objects.get(name="English").pk),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'], "antimage"))
        self.assertFalse(self.contains_player(response.context['players'], "axe"))

    def test_av_weekdays(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'av_weekdays':True,
            'roles':(Role.objects.get(name='Support').pk, Role.objects.get(name='Jungler').pk),
            'servers':(Server.objects.get(name='EU West').pk, Server.objects.get(name='Dubai').pk),
            'languages':(Language.objects.get(english_version='Spanish').pk,
                         Language.objects.get(name='English').pk),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'], 'axe'))
        self.assertFalse(self.contains_player(response.context['players'], 'enigma'))

    def test_av_weeknights(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'av_weeknights':True,
            'roles':(Role.objects.get(name='Jungler').pk,),
            'servers':(Server.objects.get(name='US West').pk, Server.objects.get(name='Dubai').pk),
            'languages':(Language.objects.get(english_version='Spanish').pk,
                         Language.objects.get(name='English').pk),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'], 'enigma'))
        self.assertFalse(self.contains_player(response.context['players'], 'tinker'))

    def test_av_weekends(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'av_weekends':True,
            'roles':(Role.objects.get(name='Jungler').pk, Role.objects.get(name='Mid').pk),
            'servers':(Server.objects.get(name='US West').pk,
                       Server.objects.get(name='EU East').pk),
            'languages':(Language.objects.get(name='English').pk),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'], 'queenofpain'))
        self.assertFalse(self.contains_player(response.context['players'], 'tinker'))

    def test_must_have_mic(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'microphone':True,
            'roles':(Role.objects.get(name='Support').pk, Role.objects.get(name='Jungler').pk),
            'servers':(Server.objects.get(name='EU West').pk, Server.objects.get(name="Dubai").pk),
            'languages':(Language.objects.get(english_version="Spanish").pk,
                         Language.objects.get(name="English").pk),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'], 'axe'))
        self.assertFalse(self.contains_player(response.context['players'], 'enigma'))

    def test_roles_filter(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles':(Role.objects.get(name='Mid').pk, ),
            'servers':(Server.objects.get(name='US West').pk,
                       Server.objects.get(name='EU East').pk),
            'languages':(Language.objects.get(name='English').pk),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'], 'queenofpain'))
        self.assertFalse(self.contains_player(response.context['players'], 'tinker'))

    def test_server_filters(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles':(Role.objects.get(name='Support').pk, Role.objects.get(name='Jungler').pk),
            'servers':(Server.objects.get(name='US West').pk, Server.objects.get(name='Dubai').pk),
            'languages':(Language.objects.get(english_version='Spanish').pk,
                         Language.objects.get(name='English').pk),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'], 'tinker'))
        self.assertTrue(self.contains_player(response.context['players'], 'enigma'))
        self.assertFalse(self.contains_player(response.context['players'], 'axe'))

    def test_languages_filters(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles':(Role.objects.get(name='Support').pk, Role.objects.get(name='Carry').pk),
            'servers':(Server.objects.get(name='EU West').pk, Server.objects.get(name="Russia").pk),
            'languages':(Language.objects.get(name="English").pk,),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'],'axe'))
        self.assertFalse(self.contains_player(response.context['players'],'antimage'))

    def test_include_in_search_option(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles':(Role.objects.get(name='Carry').pk),
            'servers':(Server.objects.get(name="Russia").pk,),
            'languages':(Language.objects.get(english_version="Russian").pk,),
            'friendliness_weight':5,
            'teamwork_weight':5,
            'reliability_weight':5,
            })

        self.assertTrue(self.contains_player(response.context['players'],'antimage'))
        self.assertFalse(self.contains_player(response.context['players'],'riki'))


class RankingTest(TestCase):
    """
    Test that search results are in the correct order
    3,5,7.. etc may be new players and therefore not in rating order
    """
    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()

    def test_max_friendliness(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles': tuple(Role.objects.all().values_list('pk',flat=True)),
            'servers':tuple(Server.objects.all().values_list('pk',flat=True)),
            'languages':tuple(Language.objects.all().values_list('pk',flat=True)),
            'friendliness_weight':10,
            'teamwork_weight':0,
            'reliability_weight':0,
            })
        ranked = [] 
        for player in response.context['players']:
            if not player.is_new():
                ranked.append((player, player.get_friendliness()))
        sorted_list = sorted(ranked, key=itemgetter(1), reverse=True)
        self.assertEqual(sorted_list, ranked)

    def test_max_teamwork(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles': tuple(Role.objects.all().values_list('pk',flat=True)),
            'servers':tuple(Server.objects.all().values_list('pk',flat=True)),
            'languages':tuple(Language.objects.all().values_list('pk',flat=True)),
            'friendliness_weight':0,
            'teamwork_weight':10,
            'reliability_weight':0,
            })
        ranked = [] 
        for player in response.context['players']:
            if not player.is_new():
                ranked.append((player, player.get_teamwork()))
        ranked_players = [p for p,_ in ranked]
        sorted_list = sorted(ranked, key=itemgetter(1), reverse=True)
        sorted_players = [p for p,_ in sorted_list]

        self.assertEqual(sorted_players, ranked_players)
    def test_max_reliability(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles': tuple(Role.objects.all().values_list('pk',flat=True)),
            'servers':tuple(Server.objects.all().values_list('pk',flat=True)),
            'languages':tuple(Language.objects.all().values_list('pk',flat=True)),
            'friendliness_weight':0,
            'teamwork_weight':0,
            'reliability_weight':10,
            })
        ranked = [] 
        for player in response.context['players']:
            if not player.is_new():
                ranked.append((player, player.get_reliability()))
        ranked_players = [p for p,_ in ranked]
        sorted_list = sorted(ranked, key=itemgetter(1), reverse=True)
        sorted_players = [p for p,_ in sorted_list]

        self.assertEqual(sorted_players, ranked_players)


    def test_combination(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles': tuple(Role.objects.all().values_list('pk',flat=True)),
            'servers':tuple(Server.objects.all().values_list('pk',flat=True)),
            'languages':tuple(Language.objects.all().values_list('pk',flat=True)),
            'friendliness_weight':2,
            'teamwork_weight':8,
            'reliability_weight':1,
            })
        ranked = [] 
        for player in response.context['players']:
            if not player.is_new():
                ranked.append((player,player.get_friendliness() * 2 + 
                                      player.get_teamwork() * 8 + 
                                      player.get_reliability() * 1))

        ranked_players = [p for p,_ in ranked]
        sorted_list = sorted(ranked, key=itemgetter(1), reverse=True)
        sorted_players = [p for p,_ in sorted_list]

        self.assertEqual(sorted_players, ranked_players)

    def test_bad_large_weight(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles': tuple(Role.objects.all().values_list('pk',flat=True)),
            'servers':tuple(Server.objects.all().values_list('pk',flat=True)),
            'languages':tuple(Language.objects.all().values_list('pk',flat=True)),
            'friendliness_weight':12,
            'teamwork_weight':8,
            'reliability_weight':1,
            })

        self.assertTrue('friendliness_weight' in response.context['form'].errors.keys())

    def test_bad_negative_weight(self):
        response = self.client.get('/players/', {
            'min_mmr':0000,
            'max_mmr':10000,
            'roles': tuple(Role.objects.all().values_list('pk',flat=True)),
            'servers':tuple(Server.objects.all().values_list('pk',flat=True)),
            'languages':tuple(Language.objects.all().values_list('pk',flat=True)),
            'friendliness_weight':12,
            'teamwork_weight':8,
            'reliability_weight':-1,
            })

        self.assertTrue('reliability_weight' in response.context['form'].errors.keys())

