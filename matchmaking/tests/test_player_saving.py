from django.test import TestCase, Client
from matchmaking.models import *
from django.contrib.auth.models import User
import json

class PlayerSaveTest(TestCase):
    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username="axe", password="password")
        PlayerSave.objects.create(
                target_player=Player.objects.get(user__username="queenofpain"),
                critic=User.objects.get(username="axe")
                )
        PlayerSave.objects.create(
                target_player=Player.objects.get(user__username="riki"),
                critic=User.objects.get(username="axe")
                )


    def test_save_player(self):
        response = self.client.post('/players/save/',
                {'target_player':Player.objects.get(user__username="tinker").pk})
        data = json.loads(response.content.decode('utf-8'))
        self.assertTrue(data['success'])
        self.assertTrue(PlayerSave.objects.filter(
            critic=User.objects.get(username="axe"),
            target_player=Player.objects.get(user__username="tinker"))\
            .exists())

    def test_forget_player(self):
        response = self.client.post('/players/forget/',
                {'target_player':Player.objects.get(user__username="riki").pk})
        data = json.loads(response.content.decode('utf-8'))
        self.assertTrue(data['success'])
        self.assertFalse(PlayerSave.objects.filter(
            critic=User.objects.get(username="axe"),
            target_player=Player.objects.get(user__username="riki"))\
            .exists())

    def test_player_saved(self):
        response = self.client.get('/players/saved/')
        self.assertTrue(Player.objects.get(user__username="queenofpain")
                in response.context['players'])
        self.assertFalse(Player.objects.get(user__username="antimage")
                in response.context['players'])





        

