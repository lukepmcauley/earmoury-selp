from django.test import TestCase, Client


class UpdatePlayerTests(TestCase):
    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username="queenofpain", password="password")

    def test_create_status_code(self):
        response = self.client.get('/players/update/')
        self.assertEqual(response.status_code, 200)


    def test_max_mmr(self):
        response = self.client.post('/players/update/', { 'mmr':10001 })

        self.assertTrue('less than or equal to 10000' in
               #flatten list
               ''.join( response.context['form'].errors['mmr'])
                )

    def test_min_mmr(self):
        response = self.client.post('/players/update/', { 'mmr':-1 })

        self.assertTrue('greater than or equal to 0' in
                #flatten list
                ''.join(response.context['form'].errors['mmr'])
                )
