from django.conf.urls import patterns, url, include

from matchmaking.views import *

urlpatterns = patterns('',
        url(r'^(?P<pk>\d+)/$', player_detail, name='player_detail'),
        url(r'^(?P<pk>\d+)/steam_stats/$', get_steam_stats, name='steam_stats'),
        url(r'^update/$', player_create_or_update, name='player_update'),
        url(r'^save/$', save_player, name='save_player'),
        url(r'^forget/$', forget_player, name='forget_player'),
        url(r'^saved/$', player_saved, name='player_saved'),
        url(r'^$', player_search, name='player_search'),
        )
