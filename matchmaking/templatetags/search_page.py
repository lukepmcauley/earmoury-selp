from django import template
from django.utils.safestring import mark_safe
import re

register = template.Library()

@register.filter
def search_page(string, arg=1):
    new, changed = re.subn("page=\d+","page=%d" % (arg), string)
    if not changed:
        new = string + "&page=%d" % (arg)
    return new

@register.filter
def tickcross(val):
    if val:
        element = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>'
    else:
        element = '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'

    return mark_safe(element)
