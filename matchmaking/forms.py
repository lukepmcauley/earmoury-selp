from django import forms
from matchmaking.models import Server, Role, Language, Player, PlayerSave

class SearchForm(forms.Form):
    min_mmr = forms.IntegerField(initial=0, min_value=0, max_value=10000, label="Min MMR")
    max_mmr = forms.IntegerField(initial=10000, min_value=0, max_value=10000, label="Max MMR")
    ranked = forms.BooleanField(initial=True, required=False, label="Playing ranked?")
    servers = forms.ModelMultipleChoiceField(queryset=Server.objects.all())
    roles = forms.ModelMultipleChoiceField(queryset=Role.objects.all())
    languages = forms.ModelMultipleChoiceField(queryset=Language.objects.all())
    microphone = forms.BooleanField(required=False, label="Needs a microphone?")
    av_weekdays = forms.BooleanField(required=False, label="Weekdays?")
    av_weeknights = forms.BooleanField(required=False, label="Weeknights?")
    av_weekends = forms.BooleanField(required=False, label="Weekends?")
    friendliness_weight = forms.IntegerField(widget=forms.NumberInput(attrs={
    'id':"slider1",
   ' data-slider-min':"0",
   ' data-slider-max':"10",
   ' data-slider-step':"1",
   ' data-slider-orientation':"horizontal",
   ' data-slider-selection':"after",
   ' data-slider-tooltip':"hide",
   }), initial=5, min_value=0, max_value=10,label="Friendliness Weight")
    teamwork_weight = forms.IntegerField(widget=forms.NumberInput(attrs={
    'id':"slider2",
   ' data-slider-min':"0",
   ' data-slider-max':"10",
   ' data-slider-step':"1",
   ' data-slider-orientation':"horizontal",
   ' data-slider-selection':"after",
   ' data-slider-tooltip':"hide",
   }),initial=5, min_value=0, max_value=10,label="Teamwork Weight")
    reliability_weight = forms.IntegerField(widget=forms.NumberInput(attrs={
    'id':"slider3",
   ' data-slider-min':"0",
   ' data-slider-max':"10",
   ' data-slider-step':"1",
   ' data-slider-orientation':"horizontal",
   ' data-slider-selection':"after",
   ' data-slider-tooltip':"hide",
   }),initial=5, min_value=0, max_value=10,label="Reliability Weight")
class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = ['include_in_search','steam_id', 'dota_buff_profile', 'mmr', 'ranked',
                  'roles', 'servers', 'languages', 'mic', 'av_weekdays', 'av_weekends',
                  'av_weeknights', 'message',]


class RatingForm(forms.Form):
    friendliness = forms.IntegerField(initial=3, min_value=0, max_value=5,
            label="", help_text="How fun they are to play with?",
            widget=forms.NumberInput(
                attrs={ 'data-min':1, 'data-max':5, 'class':"rating" }))
    teamwork = forms.IntegerField(initial=3, min_value=0, max_value=5,
            label="",
            help_text="How was their co-operation and communication in game?",
            widget=forms.NumberInput(
                attrs={ 'data-min':1, 'data-max':5, 'class':"rating" }))
    reliability= forms.IntegerField(initial=3, min_value=0, max_value=5,
            label="",
            help_text="Did they show up when needed and have a good internet connection?",
                widget=forms.NumberInput(
                    attrs={ 'data-min':1, 'data-max':5, 'class':"rating" }))

class GetPlayer(forms.ModelForm):
    """ This form is used to validate ajax sending a player pk """
    class Meta:
        model = PlayerSave
        fields = ['target_player']
