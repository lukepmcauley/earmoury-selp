import urllib.request
class SteamAPI():
    """
    steam api docs can be found at 
    https://developer.valvesoftware.com/wiki/Steam_Web_API
    """
    base = "http://api.steampowered.com/"

    def __init__(self,key):
        self.key = key

    def get_player_summaries(self, steam_ids):
        service = "ISteamUser/GetPlayerSummaries/v0002/"
        url = self.base + service + "?key=%s&steamids=" % self.key
        if 0 < len(steam_ids) < 100:
            for steam_id  in steam_ids:
                url += str(steam_id) + ","
            req = urllib.request.Request(url)
            try:
                response = urllib.request.urlopen(req)
                if response.status == 200:
                    json_data = urllib.request.urlopen(req).readall().decode('utf-8')
                    return json_data
            except:
                pass
        steam_stats = None
        return steam_stats

if __name__ == '__main__':
    api = SteamAPI("3ED04ED324827B4316D7BB69E117EF1F") 
    json = api.get_player_summaries([76561198009347080, 76561198030654385,])
    print(json)
