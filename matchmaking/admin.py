from django.contrib import admin
from matchmaking.models import *

admin.site.register(Player)
admin.site.register(Server)
admin.site.register(Role)
admin.site.register(Language)
admin.site.register(PlayerRating)
admin.site.register(PlayerSave)

